from django.db import models
from django.contrib.auth.models import AbstractUser
# from oauth2client.contrib.django_util.models import CredentialsField


class User(AbstractUser):
    bot_manager = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    active_parent = models.BooleanField(default=False)


class Location(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=400)

    def __str__(self):
        return self.location


class Bot(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    password = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    manager = models.OneToOneField(User, on_delete=models.CASCADE)
    # picture


class UnfinishedEvents(models.Model):
    event = models.CharField(max_length=500)
    thread = models.CharField(max_length=500)
#
# class CredentialsModel(models.Model):
#   id = models.ForeignKey(User, primary_key=True)
#   credential = CredentialsField()
#
