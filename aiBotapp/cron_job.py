# from django_cron import CronJobBase, Schedule


# "source  /home/msulaman/bot-env/bin/activate"

# "python manage.py my"

# import os
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "aiBotProject.settings")
# import django
# django.setup()

from aiBotapp.views import reply_in_threads
from aiBotapp.models import User
import os
import threading
import time


def my_scheduled_job():
    user_objs = User.objects.filter(is_superuser=False, bot_manager__isnull=False)
    thread_list = []
    for data in user_objs:
        thread = AppointmentThread(data.first_name, data.id)
        thread_list.append(thread)
        thread.start()
        print("Thread: "+thread.name, "Bot: "+data.first_name, "Manager: "+data.bot_manager.first_name)

    for thread in thread_list:
        thread.join()


class AppointmentThread (threading.Thread):

    def __init__(self, name, id):
        threading.Thread.__init__(self)
        self.name = name
        self.bot_id = id

    def run(self):
        try:
            reply_in_threads(request='', user_id=self.bot_id)
        except Exception as e:
            print(e)

        time.sleep(3)


# if __name__ == "__main__": my_scheduled_job()

#
# from django_cron import CronJobBase, Schedule
#
#
# class MyCronJob(CronJobBase):
#     RUN_EVERY_MINS = 1 # every 2 hours
#
#     schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
#     code = 'my_app.my_cron_job'    # a unique code
#
#     def do(self):
#         thread_list = []
#         thread = AppointmentThread('usertest', 72)
#         thread_list.append(thread)
#         thread.start()
#         print(72, 'call after')
#
#         for thread in thread_list:
#             thread.join()