from django.apps import AppConfig


class AibotappConfig(AppConfig):
    name = 'aiBotapp'
