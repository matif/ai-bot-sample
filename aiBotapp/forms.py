from django.forms import ModelForm
from aiBotapp.models import *
from django import forms
from allauth.account.adapter import DefaultAccountAdapter
from django import forms


class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label=("Email Or Username"), max_length=254)


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=("New password confirmation"),
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2


class SignupFormAi(ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password']
        widgets = {
             'password': forms.PasswordInput(),
             }

    def save(self, commit=True):
        user = super(SignupFormAi, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class AddbotForm(ModelForm):
    """add a bot """
    class Meta:
        model = Bot
        fields = ['first_name', 'last_name', 'email', 'password', 'gender']
        widgets = {
            'password': forms.PasswordInput(),
        }

# from allauth.account.forms import LoginForm
# class MyCustomLoginForm(LoginForm):
#
#     def __init__(self, *args, **kwargs):
#         print(self)
#
#     def login(self, *args, **kwargs):
#         print('ddddddd')
#         # Add your own processing here.
#
#         # You must return the original result.
#         return super(MyCustomLoginForm, self).login(*args, **kwargs)
#


from allauth.socialaccount.forms import SignupForm
class MyCustomSocialSignupForm(SignupForm):

    def save(self):
        print(self)
        # Ensure you call the parent classes save.
        # .save() returns a User object.
        user = super(MyCustomSocialSignupForm, self).save()

        # Add your own processing here.

        # You must return the original result.
        return user