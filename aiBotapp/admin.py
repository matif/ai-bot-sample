from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User
from .views import get_user_obj
from django.contrib import admin
from django.contrib.admin.actions import delete_selected as django_delete_selected
import requests
from allauth.socialaccount.models import SocialAccount, SocialToken


def check_for_refresh_token(credentials, username):
    if credentials.refresh_token:
        creds = credentials.refresh_token
    else:
        creds = credentials.token

    return creds


def delete_selected(modeladmin, request, queryset):
    ret=django_delete_selected(modeladmin, request, queryset)

    for obj in queryset:
        try:
            credentials, username = get_user_obj(obj.pk)
            creds=check_for_refresh_token(credentials, username)

            requests.post('https://accounts.google.com/o/oauth2/revoke',
                          params={'token': creds},
                          headers={'content-type': 'application/x-www-form-urlencoded'})

            if not obj.bot_manager_id:
                manager_bot=User.objects.filter(bot_manager_id=obj.pk)

                for bot in manager_bot:
                    credentials, username = get_user_obj(bot.pk)
                    creds = check_for_refresh_token(credentials, username)
                    requests.post('https://accounts.google.com/o/oauth2/revoke',
                                  params={'token': creds},
                                  headers={'content-type': 'application/x-www-form-urlencoded'})

        except :
            continue

    return ret


class AuthorAdmin(admin.ModelAdmin):
    actions = [delete_selected]

    def delete_model(self, request, obj):
        try:
            credentials, usernmae = get_user_obj(obj.pk)
            if credentials.refresh_token:
                creds = credentials.refresh_token
            else:
                creds = credentials.token

            requests.post('https://accounts.google.com/o/oauth2/revoke',
                          params={'token': creds},
                          headers={'content-type': 'application/x-www-form-urlencoded'})

            if not obj.bot_manager_id:
                manager_bot=User.objects.filter(bot_manager_id=obj.pk)

                for bot in manager_bot:
                    credentials, username = get_user_obj(bot.pk)
                    creds = check_for_refresh_token(credentials, username)
                    requests.post('https://accounts.google.com/o/oauth2/revoke',
                                  params={'token': creds},
                                  headers={'content-type': 'application/x-www-form-urlencoded'})

        except SocialAccount.DoesNotExist:
            return super(AuthorAdmin, self).delete_model(request, obj)

        return super(AuthorAdmin, self).delete_model(request, obj)


admin.site.register(User, AuthorAdmin)