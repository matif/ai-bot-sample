from django.conf.urls import include, url
from django.contrib import admin
from . import views
from aiBotapp.forgotPassword import ResetPasswordRequestView, PasswordResetConfirmView


try:
    from django.conf.urls import patterns, include, url

    urlpatterns = patterns('',
                           url(r'^admin/', admin.site.urls),
                           url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
                               PasswordResetConfirmView.as_view(), name='reset_password_confirm'),
                           # PS: url above is going to used for next section of
                           # implementation.
                           url(r'^account/reset_password',
                               ResetPasswordRequestView.as_view(), name="reset_password")
                           )

except:
    from django.conf.urls import include, url

    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
            PasswordResetConfirmView.as_view(), name='reset_password_confirm'),
        # PS: url above is going to used for next section of
        # implementation.
        url(r'^account/reset_password',
            ResetPasswordRequestView.as_view())
    ]

urlpatterns += [
    # signup url
    url(r'^signup/', views.signUp, name='signup'),
    url(r'^login/', views.login_user, name='login'),
    url(r'^home/', views.home_page, name='home'),
    url(r'^logout/', views.logout_view, name='logout'),
    url(r'^check/', views.checkrasa, name='check_rasa'),
    url(r'^deleteBot/', views.deleteBot, name='deleteBot'),
    url(r'^addBot/', views.addBot, name='addbot'),
    url(r'^inbox/(?P<user_id>\d+)/$', views.user_inbox_emails, name='inbox'),
    url(r'^intentions/(?:(?P<report_type>.+)/)?', views.check_intentions, name='check_intentions'),
    url(r'^intention_action/(?P<intention>.*)/$', views.intention_action, name='intention_action'),


    url(r'^entities/', views.entities_finding_rasa, name='entities'),
    url(r'^reply_in_threads/(?P<user_id>\d+)/', views.reply_in_threads, name='reply_in_threads'),

]
