from aiBotapp.models import User

# from allauth.account.models import EmailAccount
# from allauth.exceptions import ImmediateHttpResponse
# from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
# from allauth.account.auth_backends import AuthenticationBackend
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.adapter import get_adapter as get_account_adapter
from django.core.cache import cache
from allauth.account.forms import LoginForm


class UserSaveAdapter(DefaultSocialAccountAdapter):

    def get_provider_config(self, request, provider):
        print('dat')

    def save_user(self, request, sociallogin, form=None):
        # This method is to save data in user table for social login
        u = sociallogin.user
        u.set_unusable_password()
        if form:
            get_account_adapter().save_user(request, u, form)
        else:
            if cache.get('manager_user', None):
                u.username = str(u.first_name)+'_'+str(u.last_name)
                u.bot_manager = cache.get('manager_user')

        sociallogin.save(request)
        return u

    def pre_social_login(self, request, sociallogin):
            user = sociallogin.user
            if user.id:
                return
            try:
                customer = User.objects.get(email=user.email)  # if user exists, connect the account to the existing account and login
                sociallogin.state['process'] = 'connect'
                # perform_login(request, customer, 'none')
            except User.DoesNotExist:
                    pass
